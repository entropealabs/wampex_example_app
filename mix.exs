defmodule WampexExampleApp.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_example_app,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {WampexExampleApp.Application, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [wampex_example_app: :permanent, runtime_tools: :permanent],
        cookie: "wampex_example_app_cookie"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:wampex_client, path: "../wampex_client"}
      {:wampex_client,
       git: "https://gitlab.com/entropealabs/wampex_client.git",
       tag: "a6478667525bbc048215a94ec237a6b45f9009f1"}
    ]
  end
end
