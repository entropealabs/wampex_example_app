# WampexExampleApp

A small application to demo the WAMPex library.

This emulates a smart home with a few actuators and sensors, and a dashboard for collecting sensor readings and controlling the actuators.

In [lib/wampex_example_app/application.ex](lib/wampex_example_app/application.ex) you can see we are starting two WAMPex Sessions. One is called `Device`, you could imagine this as being a processs running on embedded device running Nerves or Grisp. It could just as easily be an embedded C++ device utilizing a native WAMP client as well.

The `Controller` Wampex session would be utilized by a web service, web client or another remote device. This could also utilize a Javascript WAMP client to run in the browser and subscribe to topics and control the actuators on the `Device`.

In reality each Peer is equal and can utilize all four roles, a Callee as well as a Caller, and a Publisher as well as a Subscriber.

## Running

### WAMP Router

We're using [WAMPexRouter](https://gitlab.com/entropealabs/wampex_router) as our WAMP Router.

WAMPexRouter itself is just a library, so there's a simple example application to run a small cluster using docker-compose.

Follow the directions at [wampex_example_router](https://gitlab.com/entropealabs/wampex_example_router) to get a small cluster running. Be sure to make note of the _FIRST_ password created.

### Example App

In a new terminal, let's run our Elixir app.

```bash
$ git clone https://gitlab.com/entropealabs/wampex_example_app.git
$ cd wampex_example_app
$ asdf install
$ mix deps.get
```

We need to make the password available from our bootstrapped cluster, update the `SECRET` variable in `default.env` with the _FIRST_ one output when you started the cluster.

next
```bash
$ source default.env
$ iex -S mix
```

You should now see some events and rpc calls coming through.

### Sample Output

```bash
23:18:14.693 [info]  [SmartHome] - Setting level.2.room.3.actuator.light.1 to <<1>>

23:18:14.696 [info]  [Dashboard] - Response from level.2.room.3.actuator.light.1: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:14.697 [info]  [SmartHome] - Setting level.1.room.5.actuator.light.3 to <<0>>

23:18:14.698 [info]  [SmartHome] - Setting level.1.room.5.actuator.fan.1 to <<1>>

23:18:14.700 [info]  [Dashboard] - Response from level.1.room.5.actuator.light.3: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:14.741 [info]  [Dashboard] - Response from level.1.room.5.actuator.fan.1: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.105 [info]  [Dashboard] - Received sensor reading {:event, 4852795090901289, 3764336341734956, %{"publisher" => 1410423894225726, "timestamp" => 1582003095099, "topic" => "level.1.room.5.sensor.temperature.1"}, <<22, 21, 23, 24, 20>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.106 [info]  [Dashboard] - Received sensor reading {:event, 5222484242267678, 8549810542825208, %{"publisher" => 1410423894225726, "timestamp" => 1582003095101, "topic" => "level.1.room.4.sensor.temperature.1"}, <<22, 24, 24, 21, 20>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.107 [info]  [Dashboard] - Received sensor reading {:event, 6245040978976813, 3059263023607559, %{"publisher" => 1410423894225726, "timestamp" => 1582003095102, "topic" => "level.1.room.4.sensor.humidity.1"}, <<23, 20, 25, 20, 24>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.395 [info]  [SmartHome] - Setting level.2.room.3.actuator.light.1 to <<1>>

23:18:15.399 [info]  [Dashboard] - Response from level.2.room.3.actuator.light.1: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.399 [info]  [SmartHome] - Setting level.1.room.5.actuator.light.3 to <<0>>

23:18:15.400 [info]  [SmartHome] - Setting level.1.room.5.actuator.fan.1 to <<1>>

23:18:15.402 [info]  [Dashboard] - Response from level.1.room.5.actuator.light.3: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.446 [info]  [Dashboard] - Response from level.1.room.5.actuator.fan.1: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.606 [info]  [Dashboard] - Received sensor reading {:event, 4852795090901289, 6780710109982530, %{"publisher" => 1410423894225726, "timestamp" => 1582003095601, "topic" => "level.1.room.5.sensor.temperature.1"}, <<24, 22, 25, 20, 23>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.609 [info]  [Dashboard] - Received sensor reading {:event, 5222484242267678, 3188665040125472, %{"publisher" => 1410423894225726, "timestamp" => 1582003095601, "topic" => "level.1.room.4.sensor.temperature.1"}, <<21, 20, 20, 25, 25>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:15.612 [info]  [Dashboard] - Received sensor reading {:event, 6245040978976813, 726630032470439, %{"publisher" => 1410423894225726, "timestamp" => 1582003095603, "topic" => "level.1.room.4.sensor.humidity.1"}, <<25, 25, 22, 21, 25>>, %{"lat" => 41.878113, "long" => -87.629799}}

23:18:16.096 [info]  [SmartHome] - Setting level.2.room.3.actuator.light.1 to <<0>>

23:18:16.099 [info]  [Dashboard] - Response from level.2.room.3.actuator.light.1: {:ok, ["ok"], %{"lat" => 41.878113, "long" => -87.629799}}
```

Happy hacking ;)
