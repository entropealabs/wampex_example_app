# Device Registration
A high level overview of the life cycle of a new widget.

## Actors
* Widget
  * A network connected device with a unique ID eg; MAC Address
* Manufacturer
  * Manufacturer of the Widget
* Supplier
  * Receives widget from manufacturer and sells it to Customer
* Customer
  * Owner of widget after purchase from Supplier
* Platform
  * Manages IoT Infrastructure eg; Control Plane, Registration Service and Dashboard
* Control Plane
  * WAMP Router
* Registration Service
  * Proxy to WAMP Admin
  * WAMP Client
  * Stores Widget ID and Customer ID relationship
* Dashboard
  * Customer facing Widget interface

## Widget Registration

![Widget Registration](./assets/widget-registration.mm.png)

## Customer Registration

![Customer Registration](./assets/customer-registration.mm.png)
