defmodule WampexExampleApp do
  @moduledoc """
  Documentation for WampexExampleApp.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WampexExampleApp.hello()
      :world

  """
  def hello do
    :world
  end
end
