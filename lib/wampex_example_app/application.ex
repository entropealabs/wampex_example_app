defmodule WampexExampleApp.Application do
  @moduledoc false
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Callee, Caller, Publisher, Subscriber}
  alias WampexExampleApp.SmartHome

  require Logger

  use Application

  def start(_type, _args) do
    url = System.get_env("WAMP_SERVER")
    realm = System.get_env("REALM")
    authid = System.get_env("AUTHID")
    secret = System.get_env("SECRET")

    authentication = %Authentication{
      authid: authid,
      authmethods: ["wampcra"],
      secret: secret
    }

    realm = %Realm{name: realm, authentication: authentication}

    device_roles = [Callee, Caller, Publisher, Subscriber]
    device_session = %Session{url: url, realm: realm, roles: device_roles}

    children = [
      {Client, name: Device, session: device_session, reconnect: true},
      {SmartHome, client: Device}
    ]

    opts = [strategy: :one_for_one, name: WampexExampleApp.Supervisor, max_restarts: 100]
    Supervisor.start_link(children, opts)
  end
end
