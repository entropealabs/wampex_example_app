defmodule WampexExampleApp.SensorLogger do
  @moduledoc false
  use GenServer
  require Logger
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Subscriber.{Subscribe}

  def start_link([name, sensors]) do
    GenServer.start_link(__MODULE__, {name, sensors})
  end

  def init(state) do
    {:ok, state, {:continue, :ok}}
  end

  def handle_continue(:ok, {name, sensors}) do
    sensors = subscribe_sensors(name, sensors)
    {:noreply, {name, sensors}}
  end

  def subscribe_sensors(name, sensors) do
    Enum.map(sensors, fn s -> Client.subscribe(name, %Subscribe{topic: s}) end)
  end

  def handle_info(%Event{arg_list: arg_list, arg_kw: arg_keyword}, state) do
    Logger.info(
      "[SensorLogger] - Writing Sensor Data: #{inspect(arg_list)} #{inspect(arg_keyword)}"
    )

    {:noreply, state}
  end
end
