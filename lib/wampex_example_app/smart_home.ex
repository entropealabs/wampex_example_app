defmodule WampexExampleApp.SmartHome do
  @moduledoc false
  use GenServer
  require Logger
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.Result
  alias Wampex.Roles.Publisher.Publish
  alias Wampex.Roles.Subscriber.Subscribe

  @sensor_interval 319

  def start_link(client: client) do
    GenServer.start_link(__MODULE__, client)
  end

  @impl true
  def init(client) do
    id = "FF3489GG759384DD75398475"
    {:ok, %{client: client, id: id}, {:continue, :ok}}
  end

  @impl true
  def handle_continue(:ok, %{client: client} = state) do
    Client.add(client, self())
    {:noreply, state}
  end

  @impl true
  def handle_info({:connected, _client}, %{client: client, id: id} = state) do
    resp = do_connect(client, id)
    Logger.info("Connected: #{inspect(resp)}")
    Process.send_after(self(), :take_reading, 0)
    Process.send_after(self(), :set_attributes, 0)
    Process.send_after(self(), :get_data, 0)
    add_alerts(client, alerts())
    subscribe_alerts(client)
    {:noreply, state}
  end

  @impl true
  def handle_info(:take_reading, %{client: client, id: id} = state) do
    data = %Publish{topic: "rosetta.#{id}", arg_kw: %{id: id, data: get_data()}}

    Client.publish(
      client,
      data
    )

    Process.send_after(self(), :take_reading, @sensor_interval)
    {:noreply, state}
  end

  @impl true
  def handle_info(:get_data, %{client: client, id: id} = state) do
    to = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    from = NaiveDateTime.add(to, -3600, :second)

    res =
      Client.call(client, %Call{
        procedure: "telemetry.get_metrics",
        arg_kw: %{
          from_date: NaiveDateTime.to_iso8601(from),
          to_date: NaiveDateTime.to_iso8601(to),
          bucket: 5 * 60,
          metrics: ["rosetta.#{id}.air_quality.co2", "rosetta.#{id}.air_quality.pm"]
        }
      })

    Logger.info("Metrics Data: #{inspect(res)}")

    res1 =
      Client.call(client, %Call{
        procedure: "telemetry.get_events",
        arg_kw: %{
          from_date: NaiveDateTime.to_iso8601(from),
          to_date: NaiveDateTime.to_iso8601(to),
          bucket: 5 * 60,
          events: ["rosetta.#{id}.hvac.chiller.status", "rosetta.#{id}.hvac.boiler.status"]
        }
      })

    Logger.info("Events Data: #{inspect(res1)}")
    Process.send_after(self(), :get_data, 60_000)
    {:noreply, state}
  end

  @impl true
  def handle_info(:set_attributes, %{client: client, id: id} = state) do
    Client.call(
      client,
      %Call{
        procedure: "device.set_attributes",
        arg_kw: %{id: id},
        arg_list: [["name", "The Clubhouse"], ["location", %{lat: 42.594300, lng: -86.100280}]]
      },
      500
    )

    res1 = Client.call(client, %Call{procedure: "device.get_attributes", arg_kw: %{id: id}}, 500)

    res2 = Client.call(client, %Call{procedure: "device.get_profile", arg_kw: %{id: id}}, 500)

    Logger.info("Attributes: #{inspect(res1)}")
    Logger.info("Profile: #{inspect(res2)}")
    {:noreply, state}
  end

  @impl true
  def handle_info(%Event{arg_kw: notif}, state) do
    Logger.warn("!! ALERT !! - #{inspect(notif)}")
    {:noreply, state}
  end

  @impl true
  def handle_info(event, state) do
    Logger.info("Got info event #{inspect(event)}")
    {:noreply, state}
  end

  def add_alerts(client, alerts) do
    Enum.each(alerts, fn alert ->
      resp = Client.call(client, %Call{procedure: "alerts.add", arg_kw: alert})
      Logger.info("Alert Added: #{inspect(resp)}")
    end)
  end

  def subscribe_alerts(client) do
    Client.subscribe(client, %Subscribe{topic: "device.alert"})
  end

  defp do_connect(client, id) do
    case Client.call(
           client,
           %Call{
             procedure: "device.connect",
             arg_kw: %{id: id, profile: get_profile()}
           }
         ) do
      %Result{} = res ->
        res

      er ->
        Logger.info("Can't connect to device shadow: #{inspect(er)}")
        :timer.sleep(1000)
        do_connect(client, id)
    end
  end

  def get_profile do
    %{
      version: "1.0.0",
      namespace: "org.entropealabs",
      displayName: "Clubhouse",
      description: "The Clubhouse",
      categories: ["smart_home", "hvac", "furnace", "weather", "air_quality", "lights"],
      system: %{
        manufacturer: "Entropealabs",
        type: "rosetta",
        model: "beta"
      },
      topics: [
        %{
          topic: "{{system.type}}.{{id}}",
          payload: %{}
        }
      ],
      procedures: [
        %{
          procedure: "{{system.type}}.{{id}}.light.1",
          arguments: %{}
        }
      ],
      metric_parsers: %{
        "{{system.type}}.{{id}}" => [
          %{key: "hvac.temp", path: "$.hvac.temp"},
          %{key: "air_quality.co2", path: "$.air_quality.co2"},
          %{key: "air_quality.voc", path: "$.air_quality.voc"},
          %{key: "air_quality.pm", path: "$.air_quality.pm"}
        ]
      },
      event_parsers: %{
        "{{system.type}}.{{id}}" => [
          %{key: "light.status", path: "$.lights[*].status", list: true},
          %{key: "light.color", path: "$.lights[*].color", list: true},
          %{key: "hvac.fan_status", path: "$.hvac.fan.status"},
          %{key: "hvac.chiller.status", path: "$.hvac.chiller.status"},
          %{key: "hvac.boiler.status", path: "$.hvac.boiler.status"}
        ]
      }
    }
  end

  def get_data do
    %{
      lights: [
        %{
          status: Enum.random([:on, :off]),
          color: "#5500CC"
        },
        %{
          status: Enum.random([:on, :off]),
          color: "#5500CC"
        }
      ],
      hvac: %{
        temp: Enum.random(10..45),
        fan: %{
          status: Enum.random([:on, :off]),
          speed: Enum.random(0..1000)
        },
        chiller: %{
          status: Enum.random([:on, :off])
        },
        boiler: %{
          status: Enum.random([:on, :off])
        }
      },
      air_quality: %{
        co2: Enum.random(399..2000),
        voc: Enum.random(10..40),
        pm: Enum.random(3..10)
      }
    }
  end

  def alerts do
    [
      %{
        realm: "org.entropealabs.clubhouse",
        type: "metric",
        name: "VOC Levels",
        window: 15_000,
        aggregation: "avg",
        level: "guarded",
        path: "$.rosetta.*.air_quality.voc",
        function: "gt",
        trigger: 25
      },
      %{
        realm: "org.entropealabs.clubhouse",
        type: "event",
        name: "Chiller Status",
        window: 7_000,
        level: "guarded",
        path: "$.rosetta.*.hvac.chiller.status",
        event: "off",
        function: "gt",
        trigger: 10
      },
      %{
        realm: "org.entropealabs.clubhouse",
        type: "metric",
        name: "CO2 Levels",
        level: "guarded",
        window: 30_000,
        aggregation: "avg",
        path: "$.rosetta.*.air_quality.co2",
        function: "gt",
        trigger: 1100
      }
    ]
  end
end
