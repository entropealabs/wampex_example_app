defmodule WampexExampleApp.Dashboard do
  @moduledoc false
  use GenServer
  require Logger
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.{Caller}
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Subscriber.{Subscribe}

  @button_interval 666

  def start_link([name, actuators, sensors]) do
    GenServer.start_link(__MODULE__, {name, actuators, sensors})
  end

  def init(state) do
    {:ok, state, {:continue, :ok}}
  end

  def handle_continue(:ok, {name, actuators, sensors}) do
    sensors = subscribe_sensors(name, sensors)
    start_buttons()
    {:noreply, {name, actuators, sensors}}
  end

  def subscribe_sensors(name, sensors) do
    Enum.map(sensors, fn s -> Client.subscribe(name, %Subscribe{topic: s}) end)
  end

  def start_buttons do
    Process.send_after(self(), :press_buttons, 0)
  end

  def handle_info(:press_buttons, {name, actuators, _s} = state) do
    Enum.each(actuators, fn a ->
      Task.start(fn ->
        resp =
          Client.send_request(
            name,
            Caller.call(%Call{procedure: a, arg_list: [Enum.random(0..1)], arg_kw: %{}}),
            1000
          )

        Logger.info("Button Responded: #{inspect(resp)}")
      end)
    end)

    Process.send_after(self(), :press_buttons, @button_interval)
    {:noreply, state}
  end

  def handle_info(%Event{} = event, state) do
    Logger.info("[Dashboard] - Received sensor reading #{inspect(event)}")
    {:noreply, state}
  end
end
