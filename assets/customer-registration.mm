sequenceDiagram
    participant W as Widget
    participant C as Customer
    participant P as Platform
    participant CP as Control Plane
    participant RS as Registration Service

    W ->> C: Customer receives Widget
    W ->> CP: REGISTER: com.widget.1234567.button
    C ->> P: GET: /registration-form
    P ->> C: form-registration.html
    C ->> P: POST: un, pw, Reference ID
    P ->> CP: Admin API: create User
    P ->> CP: Call: com.widget.register_user
    CP ->> RS: {user_id: me@email.com, ref_id: 98765}
    RS ->> CP: YIELD: ok
    CP ->> P: ok
    P ->> C: login.html
    C ->> CP: HELLO: com.user.login
    CP ->> C: CHALLENGE
    C ->> CP: AUTHENTICATE
    CP ->> C: WELCOME
    C ->> CP: CALL: com.user.widgets
    CP ->> RS: {user_id: me@email.com}
    RS ->> CP: {widgets: [1234567]}
    CP ->> C: {widgets: [1234567]}
    C ->> CP: SUBSCRIBE: prefix: com.widget.1234567
    C ->> CP: CALL: com.widget.1234567.button
    CP ->> W: CALL: com.widget.1234567.button
    W ->> CP: ok
    CP ->> C: ok
    W ->> CP: PUBLISH: com.widget.1234567.sensor
    CP ->> C: EVENT: com.widget.1234567.sensor
