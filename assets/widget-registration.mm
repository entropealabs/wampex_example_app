sequenceDiagram
    participant M as Manufacturer
    participant W as Widget
    participant S as Supplier
    participant P as Platform
    participant CP as Control Plane
    participant RS as Registration Service

    M ->> W: Widget is created, firmware contains PSK
    W ->> S: Widget is shipped to Supplier
    S ->> P: POST: Supplier registers Widget
    P ->> CP: CALL: com.widget.register
    CP ->> RS: {widget_id: 1234567}
    RS ->> P: YIELD: Reference ID
    P ->> S: 98765
    S ->> S: QR Codes printed
