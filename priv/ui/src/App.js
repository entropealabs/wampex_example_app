import React from 'react';
import logo from './logo.svg';
import './App.css';
import autobahn from 'autobahn';

var pass = "bnASR5qF9y8k/sHF6S+NneCOhvVI0zFkvoKQpc2F+hA=";

function onchallenge(session, method, extra){
  console.log("onchallenge", extra.challenge);
  console.log("authenticating via '" + method + "' and challenge '" + extra.challenge + "'");
  var key = autobahn.auth_cra.derive_key(pass, "" + extra.salt + "", extra.iterations, extra.keylen);
	var sign = autobahn.auth_cra.sign(key, "" + extra.challenge + "");
  return sign; 
}

var connection = new autobahn.Connection({
  url: 'ws://localhost:4000/ws', 
  realm: 'admin',
  authid: 'admin',
  authmethods: ['wampcra'],
  onchallenge: onchallenge
});

connection.onopen = function (session) {
   // 1) subscribe to a topic
  function onevent(args, args_kw, o) {
    console.log("Event:", args);
		console.log("KW", args_kw);
		console.log("O", o);
  }
  session.subscribe('level.1.room.4.sensor.temperature.1', onevent);
  session.subscribe('level.1.room.5.sensor.temperature.1', onevent);
  session.subscribe('level.1.room.4.sensor.humidity.1', onevent);
};

connection.onclose = function(reason, dets){
  console.log(reason);
  console.log(dets);
}

connection.open();

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
